# Settings.py
# Author: EMERY Alexandre
# Date: 10/03/2024

# Pre-traitement
ENABLED_DIFFUSION = False
FILTER = None # Options: 'gaussian', 'median'
MORPH_OPERATION = "dilation" # Options: 'opening', 'closing', 'erosion', 'dilation'

# Model configuration
MODEL_NAME = 'Unet'  # Options: 'Unet', 'SegNet', 'DeepLab'

# Training settings
IMAGE_WIDTH_SIZE = 128
IMAGE_HEIGHT_SIZE = 128
BATCH_SIZE = 16
EPOCHS = 30
LR = 1e-4
DATASET_PATH = './dataset/'

# Training settings specify for U-Net and SegNet Model
NB_INIT_FILTER = 16

SMOOTH = 1e-15
