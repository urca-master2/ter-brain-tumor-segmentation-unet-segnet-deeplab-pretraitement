# Main.py
# Author: EMERY Alexandre
# Date: 10/03/2024

import os
import numpy as np
from tensorflow.keras.callbacks import ModelCheckpoint, CSVLogger
from tensorflow.keras.callbacks import ReduceLROnPlateau, EarlyStopping
from tensorflow.keras.optimizers import Adam

# Import configurations
from Settings import *

# Import model constructors
from Models.UnetModel import UnetModel
from Models.SegNetModel import SegNetModel
from Models.DeepLabModel import DeepLabModel

# Import utilities and metrics
from Metrics.Metrics import generateModelMetrics
from Utils.DatasetBuilder import DatasetBuilder
from Models.Evaluate import ModelEvaluator
from Metrics.DiceMetrics import DiceMetrics

def main():
    """
    Main function to train and evaluate a model for image segmentation.
    """
    np.random.seed(42)
    
    model_path = os.path.join("files", "model.keras")
    csv_path = os.path.join("files", "log.csv")
    
    builder = DatasetBuilder(IMAGE_WIDTH_SIZE, IMAGE_HEIGHT_SIZE)
    
    if MODEL_NAME == 'Unet':
        model_instance = UnetModel((IMAGE_HEIGHT_SIZE, IMAGE_WIDTH_SIZE, 3))
        model = model_instance.build_model(NB_INIT_FILTER)
    elif MODEL_NAME == 'DeepLab':
        model_instance = DeepLabModel((IMAGE_HEIGHT_SIZE, IMAGE_WIDTH_SIZE, 3))
        model = model_instance.build_model()
    elif MODEL_NAME == 'SegNet':
        model_instance = SegNetModel((IMAGE_HEIGHT_SIZE, IMAGE_WIDTH_SIZE, 3))
        model = model_instance.build_model(NB_INIT_FILTER)
    else:
        raise ValueError("Unsupported model specified in Settings.py")
    
    model.summary()
    
    (train_x, train_y), (valid_x, valid_y), (test_x, test_y) = builder.load_dataset(DATASET_PATH)
    train_dataset = builder.tf_dataset(train_x, train_y, batch=BATCH_SIZE)
    valid_dataset = builder.tf_dataset(valid_x, valid_y, batch=BATCH_SIZE)
    
    model.compile(
        loss=DiceMetrics.dice_loss,
        optimizer=Adam(LR),
        metrics=[DiceMetrics.dice_coef,'accuracy']
    )
        
    callbacks = [
        ModelCheckpoint(model_path, verbose=1, save_best_only=True),
        ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=5, min_lr=1e-7, verbose=1),
        CSVLogger(csv_path),
        EarlyStopping(monitor='val_loss', patience=20, restore_best_weights=False),
    ]
    
    model.fit(
        train_dataset,
        epochs=EPOCHS,
        validation_data=valid_dataset,
        callbacks=callbacks,
        verbose=0,
    )
        
    generateModelMetrics()
    
    model_evaluator = ModelEvaluator(model, IMAGE_WIDTH_SIZE, IMAGE_HEIGHT_SIZE)
    model_evaluator.evaluate_model(test_x, test_y)
    
if __name__ == "__main__":
    main()