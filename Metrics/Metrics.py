import pandas as pd
import matplotlib.pyplot as plt

def generateModelMetrics():
    """
    Loads model metrics from a CSV file, displays and saves plots of training and validation metrics,
    including Dice coefficient, accuracy, and loss.

    The plots are saved in the 'results' directory as PNG images.
    """
    # Load metrics from a CSV file
    metrics = pd.read_csv("./files/log.csv")

    # Plot for Dice coefficient
    plt.figure()
    metrics[['dice_coef','val_dice_coef']].plot()
    plt.title('Dice Coefficient - Training vs Validation')
    plt.ylabel('Dice Coefficient')
    plt.xlabel('Epoch')
    plt.savefig('./Metrics/Images/dice-coef.png')
    plt.close()

    # Plot for accuracy
    plt.figure()
    metrics[['accuracy','val_accuracy']].plot()
    plt.title('Accuracy - Training vs Validation')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.savefig('./Metrics/Images/accuracy.png')
    plt.close()

    # Plot for loss
    plt.figure()
    metrics[['loss','val_loss']].plot()
    plt.title('Loss - Training vs Validation')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.savefig('./Metrics/Images/loss.png')
    plt.close()

    print("Plots saved in the 'images' directory.")
