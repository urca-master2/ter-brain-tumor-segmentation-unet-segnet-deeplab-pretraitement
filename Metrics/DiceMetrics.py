# DiceMetrics.py
# Author: EMERY Alexandre
# Date: 10/03/2024

from tensorflow.keras.layers import Flatten
import tensorflow as tf

from Settings import *

class DiceMetrics:
    """
    A class for calculating Dice coefficient and Dice loss for model training.
    """
    
    @staticmethod
    def dice_coef(y_true, y_pred):
        """
        Calculates the Dice Coefficient.

        Parameters:
        - y_true: The ground truth labels.
        - y_pred: The predicted labels.

        Returns:
        - The Dice Coefficient.
        """
        y_true = Flatten()(y_true)
        y_pred = Flatten()(y_pred)
        intersection = tf.reduce_sum(y_true * y_pred)
        return (2. * intersection + SMOOTH) / (tf.reduce_sum(y_true) + tf.reduce_sum(y_pred) + SMOOTH)

    @staticmethod
    def dice_loss(y_true, y_pred):
        """
        Calculates the Dice Loss, useful for training segmentation models.

        Parameters:
        - y_true: The ground truth labels.
        - y_pred: The predicted labels.

        Returns:
        - The Dice Loss.
        """
        return 1.0 - DiceMetrics.dice_coef(y_true, y_pred)
