# UnetModel.py
# Author: EMERY Alexandre
# Date: 10/03/2024

import tensorflow as tf

class UnetModel:
    """
    UnetModel is a class for constructing a U-Net model for semantic segmentation.
    This model architecture is a convolutional neural network for image segmentation that
    includes an encoder-decoder structure, with skip connections between the encoder
    and decoder blocks.

    Attributes:
        input_shape (tuple): The shape of the input images.
    """

    def __init__(self, input_shape):
        """
        Initializes the U-Net model with the given input shape.

        Parameters:
            input_shape (tuple): The shape of the input images, including the number of channels.
        """
        self.input_shape = input_shape

    def conv_block(self, inputs, num_filters):
        """
        Creates a convolutional block for the U-Net model.

        Parameters:
            inputs (tf.Tensor): The input tensor for the convolutional block.
            num_filters (int): The number of filters for the convolutional layers.

        Returns:
            tf.Tensor: The output tensor of the convolutional block.
        """
        x = tf.keras.layers.Conv2D(num_filters, 3, padding="same")(inputs)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.Activation("relu")(x)
        
        x = tf.keras.layers.Conv2D(num_filters, 3, padding="same")(x)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.Activation("relu")(x)
        
        return x

    def encoder_block(self, inputs, num_filters):
        """
        Creates an encoder block for the U-Net model.

        Parameters:
            inputs (tf.Tensor): The input tensor for the encoder block.
            num_filters (int): The number of filters for the convolutional layers in the block.

        Returns:
            tuple: A tuple containing:
                - The output tensor of the last convolutional layer (skip connection).
                - The output tensor after max pooling.
        """
        x = self.conv_block(inputs, num_filters)
        p = tf.keras.layers.MaxPooling2D((2, 2))(x)
        return x, p

    def decoder_block(self, inputs, skip_features, num_filters):
        """
        Creates a decoder block for the U-Net model.

        Parameters:
            inputs (tf.Tensor): The input tensor for the decoder block.
            skip_features (tf.Tensor): The tensor from the corresponding encoder block for the skip connection.
            num_filters (int): The number of filters for the convolutional layers in the block.

        Returns:
            tf.Tensor: The output tensor of the decoder block.
        """
        x = tf.keras.layers.Conv2DTranspose(num_filters, (2, 2), strides=2, padding="same")(inputs)
        x = tf.keras.layers.concatenate([x, skip_features])
        x = self.conv_block(x, num_filters)
        return x

    def build_model(self, NB_INIT_FILTER=16):
        """
        Builds the U-Net model.

        Parameters:
            NB_INIT_FILTER (int, optional): The number of initial filters for the first convolutional layer.
                Defaults to 16.

        Returns:
            tf.keras.Model: The constructed U-Net model.
        """
        inputs = tf.keras.Input(self.input_shape)

        s1, p1 = self.encoder_block(inputs, NB_INIT_FILTER)
        s2, p2 = self.encoder_block(p1, NB_INIT_FILTER * 2)
        s3, p3 = self.encoder_block(p2, NB_INIT_FILTER * 4)
        s4, p4 = self.encoder_block(p3, NB_INIT_FILTER * 8)

        b1 = self.conv_block(p4, NB_INIT_FILTER * 16)

        d1 = self.decoder_block(b1, s4, NB_INIT_FILTER * 8)
        d2 = self.decoder_block(d1, s3, NB_INIT_FILTER * 4)
        d3 = self.decoder_block(d2, s2, NB_INIT_FILTER * 2)
        d4 = self.decoder_block(d3, s1, NB_INIT_FILTER)

        outputs = tf.keras.layers.Conv2D(1, (1, 1), padding="same", activation="sigmoid")(d4)
        model = tf.keras.Model(inputs, outputs, name="UNET")

        return model