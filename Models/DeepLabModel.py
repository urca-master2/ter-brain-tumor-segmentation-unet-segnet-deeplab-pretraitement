# DeepLabModel.py
# Author: EMERY Alexandre
# Date: 10/03/2024

import tensorflow as tf

class DeepLabModel:
    """
    DeepLabModel is a class for constructing a DeepLab v3 model for semantic segmentation.
    This model architecture leverages atrous convolution to capture multi-scale context and
    includes an Atrous Spatial Pyramid Pooling (ASPP) module to enhance segmentation performance.

    Attributes:
        input_shape (tuple): The shape of the input images.
    """

    def __init__(self, input_shape):
        """
        Initializes the DeepLab model with the given input shape.

        Parameters:
            input_shape (tuple): The shape of the input images, including the number of channels.
        """
        self.input_shape = input_shape

    def ASPP(self, inputs):
        """
        Atrous Spatial Pyramid Pooling (ASPP) block.

        Parameters:
            inputs (tf.Tensor): The input tensor for the ASPP block.

        Returns:
            tf.Tensor: The output tensor of the ASPP block.
        """
        # 1x1 convolution
        b0 = tf.keras.layers.Conv2D(128, (1, 1), padding='same', use_bias=False)(inputs)
        b0 = tf.keras.layers.BatchNormalization()(b0)
        b0 = tf.keras.layers.Activation('relu')(b0)

        # Atrous convolutions with different dilation rates
        b1 = tf.keras.layers.Conv2D(128, (3, 3), padding='same', dilation_rate=(6, 6), use_bias=False)(inputs)
        b1 = tf.keras.layers.BatchNormalization()(b1)
        b1 = tf.keras.layers.Activation('relu')(b1)

        # Image pooling
        b4 = tf.keras.layers.GlobalAveragePooling2D()(inputs)
        b4 = tf.keras.layers.Reshape((1, 1, -1))(b4)
        b4 = tf.keras.layers.Conv2D(128, (1, 1), padding='same', use_bias=False)(b4)
        b4 = tf.keras.layers.BatchNormalization()(b4)
        b4 = tf.keras.layers.Activation('relu')(b4)
        b4 = tf.keras.layers.UpSampling2D(size=(inputs.shape[1] // b4.shape[1], inputs.shape[2] // b4.shape[2]), interpolation='bilinear')(b4)

        x = tf.keras.layers.Concatenate()([b0, b1, b4])

        x = tf.keras.layers.Conv2D(128, (1, 1), padding='same', use_bias=False)(x)
        x = tf.keras.layers.BatchNormalization()(x)
        return tf.keras.layers.Activation('relu')(x)

    def build_model(self):
        """
        Builds the DeepLab v3 model.

        Returns:
            tf.keras.Model: The constructed DeepLab v3 model.
        """
        inputs = tf.keras.Input(self.input_shape)

        # Base model: could be a pretrained model like MobileNetV2, Xception, etc., with modified output layers
        base_model = tf.keras.applications.MobileNetV2(input_shape=self.input_shape, include_top=False, weights='imagenet')
        x = base_model(inputs)

        x = self.ASPP(x)

        # Up-sample the features to the same size as the input
        x = tf.keras.layers.UpSampling2D(size=(self.input_shape[0] // x.shape[1], self.input_shape[1] // x.shape[2]), interpolation='bilinear')(x)

        outputs = tf.keras.layers.Conv2D(1, (1, 1), padding="same", activation="sigmoid")(x)

        model = tf.keras.Model(inputs, outputs, name="DeepLabV3")

        return model
