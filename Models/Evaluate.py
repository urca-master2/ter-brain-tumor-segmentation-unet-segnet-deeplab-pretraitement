# Evaluate.py
# Author: EMERY Alexandre
# Date: 10/03/2024

import os
import numpy as np
import cv2
from tqdm import tqdm
from sklearn.metrics import f1_score, jaccard_score, recall_score, precision_score, confusion_matrix
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

class ModelEvaluator:
    """
    A class to evaluate the performance of image segmentation models.
    """

    def __init__(self, model, image_width_size, image_height_size):
        """
        Initializes the ModelEvaluator with the model and target image size.

        Parameters:
        - model: The segmentation model to be evaluated.
        - image_width_size: The width of the images after resizing.
        - image_height_size: The height of the images after resizing.
        """
        self.model = model
        self.image_width_size = image_width_size
        self.image_height_size = image_height_size
        
    def dice_coefficient(self, y_true, y_pred):
        """
        Calculate the Dice coefficient between two binary masks.
        """
        intersection = np.sum(y_true * y_pred)
        return (2. * intersection) / (np.sum(y_true) + np.sum(y_pred))

    def save_results(self, image, mask, y_pred, save_image_path):
        """
        Saves a composite image showing the original image, mask, and prediction side by side,
        with a superimposed image showing the differences between the mask and prediction.
        Text titles are displayed directly on the images to ensure they fit within the image boundaries.
        
        Parameters:
        - image: The original image as a numpy array.
        - mask: The true mask as a numpy array.
        - y_pred: The predicted mask as a numpy array.
        - save_image_path: Path where to save the composite image.
        """
        mask = np.expand_dims(mask, axis=-1)
        mask = np.concatenate([mask, mask, mask], axis=-1)

        y_pred = np.expand_dims(y_pred, axis=-1)
        y_pred = np.concatenate([y_pred, y_pred, y_pred], axis=-1) * 255
    
        overlay_mask = np.copy(image).astype(np.float32)
        overlay_mask[mask[:, :, 0] == 255] = (overlay_mask[mask[:, :, 0] == 255] * 0.5 + np.array([0, 255, 0]) * 0.5).astype(np.uint8)
        overlay_pred = np.copy(image).astype(np.float32)
        overlay_pred[y_pred[:, :, 0] == 255] = (overlay_pred[y_pred[:, :, 0] == 255] * 0.5 + np.array([255, 0, 0]) * 0.5).astype(np.uint8)
        comparison = np.maximum(overlay_mask, overlay_pred)

        line = np.ones((self.image_height_size, 10, 3)) * 255

        cat_images = np.concatenate([image, line, mask, line, y_pred, line, comparison], axis=1)
        
        # Add titles directly on each image with adjusted font size
        titles = ['Original', 'Mask', 'Prediction', 'Overlay']
        x_positions = [10, image.shape[1] + 20, 2 * image.shape[1] + 30, 3 * image.shape[1] + 40]
        for title, x in zip(titles, x_positions):
            cv2.putText(cat_images, title, (x, 25), 
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2, cv2.LINE_AA)

        # Save the composite image
        cv2.imwrite(save_image_path, cat_images)

    def save_confusion_matrix(self, y_true, y_pred, save_path, class_names):
        """
        Generates and saves a confusion matrix.

        Parameters:
        - y_true: The true labels.
        - y_pred: The model predictions.
        - save_path: Path to save the confusion matrix image.
        - class_names: Names of the classes.
        """
        cm = confusion_matrix(y_true, y_pred, labels=range(len(class_names)))
        
        plt.figure(figsize=(10, 7))
        sns.heatmap(cm, annot=True, fmt="d", cmap="Blues", xticklabels=class_names, yticklabels=class_names)
        plt.title('Confusion Matrix')
        plt.ylabel('True Label')
        plt.xlabel('Predicted Label')
        plt.savefig(save_path)
        plt.close()

    def evaluate_model(self, test_x, test_y):
        """
        Evaluates the model on the test dataset and saves the results.

        Parameters:
        - test_x: A list of paths to the test images.
        - test_y: A list of paths to the corresponding test masks.

        The method prints out the average F1 score, Jaccard index, Recall, and Precision of the model.
        It also saves the individual and average scores to a CSV file and displays the first prediction result.
        """
        SCORE = []
        all_y_true = []
        all_y_pred = []

        for x, y in tqdm(zip(test_x, test_y), total=len(test_y)):
            name = x.split("/")[-1]
            
            # Read image
            image = cv2.imread(x, cv2.IMREAD_COLOR)
            image = cv2.resize(image, (self.image_width_size, self.image_height_size))
            x_processed = image / 255.0
            x_processed = np.expand_dims(x_processed, axis=0)

            # Read mask
            mask = cv2.imread(y, cv2.IMREAD_GRAYSCALE)
            mask = cv2.resize(mask, (self.image_width_size, self.image_height_size))

            # Read Pred
            y_pred = self.model.predict(x_processed, verbose=0)[0]
            y_pred = np.squeeze(y_pred, axis=-1)
            y_pred = y_pred.astype(np.int32)

            save_image_path = os.path.join("results", name)
            self.save_results(image, mask, y_pred, save_image_path)

            mask = mask / 255.0
            mask = (mask > 0.5).astype(np.int32).flatten()
            y_pred = y_pred.flatten()

            all_y_true.extend(mask.tolist())
            all_y_pred.extend(y_pred.tolist())

            f1_value = f1_score(mask, y_pred, labels=[0, 1], average="binary")
            jac_value = jaccard_score(mask, y_pred, labels=[0, 1], average="binary")
            recall_value = recall_score(mask, y_pred, labels=[0, 1], average="binary", zero_division=0)
            precision_value = precision_score(mask, y_pred, labels=[0, 1], average="binary", zero_division=0)
            SCORE.append([name, f1_value, jac_value, recall_value, precision_value])

            # Save the confusion matrix for each image
            cm_save_path = os.path.join("./Metrics/Images/CMTmp", f"cm_{name}.png")
            self.save_confusion_matrix(mask, y_pred, cm_save_path, class_names=["Background", "Target"])

        score = [s[1:] for s in SCORE]
        score = np.mean(score, axis=0)
        print(f"F1: {score[0]:0.5f}, Jaccard: {score[1]:0.5f}, Recall: {score[2]:0.5f}, Precision: {score[3]:0.5f}")

        df = pd.DataFrame(SCORE, columns=["Image", "F1", "Jaccard", "Recall", "Precision"])
        df.to_csv("./files/score.csv", index=None)

        # Save the global confusion matrix
        global_cm_save_path = "./Metrics/Images/confusion_matrix.png"
        self.save_confusion_matrix(all_y_true, all_y_pred, global_cm_save_path, class_names=["Background", "Target"])