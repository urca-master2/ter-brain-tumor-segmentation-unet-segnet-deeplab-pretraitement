# SegNetModel.py
# Author: EMERY Alexandre
# Date: 10/03/2024

import tensorflow as tf

class SegNetModel:
    """
    SegNetModel is a class for constructing a SegNet model for semantic segmentation.
    This model architecture is based on an encoder-decoder structure with skip connections
    to capture context and fine-grained spatial information.

    Attributes:
        input_shape (tuple): The shape of the input images.
    """

    def __init__(self, input_shape):
        """
        Initializes the SegNet model with the given input shape.

        Parameters:
            input_shape (tuple): The shape of the input images, including the number of channels.
        """
        self.input_shape = input_shape

    def encoder_block(self, inputs, num_filters):
        """
        Encoder block for SegNet.

        Parameters:
            inputs (tf.Tensor): The input tensor for the encoder block.
            num_filters (int): The number of filters for the convolutional layers.

        Returns:
            tf.Tensor: The output tensor of the encoder block.
        """
        x = tf.keras.layers.Conv2D(num_filters, (3, 3), padding='same', use_bias=False)(inputs)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.Activation('relu')(x)
        x = tf.keras.layers.MaxPooling2D((2, 2), strides=(2, 2))(x)
        return x

    def decoder_block(self, inputs, num_filters):
        """
        Decoder block for SegNet.

        Parameters:
            inputs (tf.Tensor): The input tensor for the decoder block.
            num_filters (int): The number of filters for the convolutional layers.

        Returns:
            tf.Tensor: The output tensor of the decoder block.
        """
        x = tf.keras.layers.Conv2DTranspose(num_filters, (3, 3), strides=(2, 2), padding='same')(inputs)
        x = tf.keras.layers.BatchNormalization()(x)
        return tf.keras.layers.Activation('relu')(x)

    def build_model(self, NB_INIT_FILTER=16):
        """
        Builds the SegNet model.

        Returns:
            tf.keras.Model: The constructed SegNet model.
        """
        inputs = tf.keras.Input(self.input_shape)

        # Encoder
        e1 = self.encoder_block(inputs, NB_INIT_FILTER)
        e2 = self.encoder_block(e1, NB_INIT_FILTER * 2)
        e3 = self.encoder_block(e2, NB_INIT_FILTER * 4)
        e4 = self.encoder_block(e3, NB_INIT_FILTER *8)
        encoded = self.encoder_block(e4, NB_INIT_FILTER * 8)

        # Decoder
        d1 = self.decoder_block(encoded, NB_INIT_FILTER * 8)
        d2 = self.decoder_block(d1, NB_INIT_FILTER * 8)
        d3 = self.decoder_block(d2, NB_INIT_FILTER * 4)
        d4 = self.decoder_block(d3, NB_INIT_FILTER * 2)
        decoded = self.decoder_block(d4, NB_INIT_FILTER)

        outputs = tf.keras.layers.Conv2D(1, (1, 1), padding="same", activation="sigmoid")(decoded)

        model = tf.keras.Model(inputs, outputs, name="SegNet")

        return model