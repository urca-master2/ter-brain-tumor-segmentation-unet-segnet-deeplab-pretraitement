#!/bin/bash

# Function to check if a command exists
command_exists() {
    type "$1" &> /dev/null ;
}

# Use python3 if available, otherwise fall back to python
if command_exists python3; then
    PYTHON=python3
elif command_exists python; then
    PYTHON=python
else
    echo "Python is not installed"
    exit 1
fi

# Activate your Python environment if needed
# source /path/to/your/virtualenv/bin/activate

# Install required packages
$PYTHON -m pip install -r requirements.txt

# Run your main Python script
$PYTHON Main.py
