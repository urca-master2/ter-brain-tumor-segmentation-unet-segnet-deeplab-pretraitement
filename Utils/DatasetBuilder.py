# DatasetBuilder.py
# Author: EMERY Alexandre
# Date: 10/03/2024

import os
import numpy as np
import cv2
from glob import glob
from sklearn.model_selection import train_test_split
import tensorflow as tf
import matplotlib.pyplot as plt

from Settings import *

class DatasetBuilder:
    """
    DatasetBuilder class for loading, processing, and preparing datasets
    for image segmentation tasks.
    """
    
    def __init__(self, image_width, image_height):
        """
        Initializes the DatasetBuilder with the desired image size.
        
        Parameters:
        - image_width: The width of the images after resizing.
        - image_height: The height of the images after resizing.
        """
        self.image_width = image_width
        self.image_height = image_height
        
    @staticmethod
    def apply_spatial_filtering(image, filter_type='gaussian', kernel_size=(5, 5)):
        """
        Applies spatial filtering to an image using specified filter type.
        
        Parameters:
        - image: The input image as a numpy array.
        - filter_type: The type of filter to apply. Options: 'gaussian', 'median'.
        - kernel_size: The size of the kernel. Relevant for Gaussian filtering.
        
        Returns:
        - The filtered image as a numpy array.
        """
        if filter_type == 'gaussian':
            return cv2.GaussianBlur(image, kernel_size, 0)
        elif filter_type == 'median':
            # Note: median filter kernel size should be a single integer.
            return cv2.medianBlur(image, kernel_size[0])
        else:
            raise ValueError("Unsupported filter type. Choose 'gaussian' or 'median'.")
        
    @staticmethod
    def apply_morphological_operations(image, operation='opening', kernel_size=(5, 5)):
        """
        Applies morphological operations to an image to enhance structure.
        
        Parameters:
        - image: The input image as a numpy array.
        - operation: The morphological operation to apply. Options: 'opening', 'closing', 'erosion', 'dilation'.
        - kernel_size: The size of the kernel used for the operation.
        
        Returns:
        - The image after the morphological operation.
        """
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernel_size)
        
        if operation == 'opening':
            return cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
        elif operation == 'closing':
            return cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel)
        elif operation == 'erosion':
            return cv2.erode(image, kernel, iterations=1)
        elif operation == 'dilation':
            return cv2.dilate(image, kernel, iterations=1)
        else:
            raise ValueError("Unsupported morphological operation. Choose 'opening', 'closing', 'erosion', or 'dilation'.")
        

    @staticmethod
    def apply_anisotropic_diffusion(image, ITER_PARAM=15, K_PARAM=10, ALPHA_PARAM=0.02):
        """
        Applies anisotropic diffusion (also known as Perona-Malik filter) to an image.
        
        This function is used for edge-preserving smoothing. The idea is to encourage high contrast edges to 
        remain sharp while smoothing out areas with low contrast. This method is especially useful for noise 
        reduction while preserving edges in the image.
        
        Parameters:
        - image : numpy.ndarray
            The input image. Can be a grayscale or RGB image. If RGB, it will be converted to grayscale.
        - ITER_PARAM : int, optional
            The number of iterations the diffusion process will be applied. More iterations lead to a smoother image.
            Default is 15.
        - K_PARAM : int or float, optional
            The conductance coefficient which controls the extent of diffusion. Lower values limit diffusion across 
            edge-like regions more strongly.
            Default is 10.
        - ALPHA_PARAM : float, optional
            The integration constant specifying the jump size at each iteration, effectively controlling the speed 
            of diffusion.
            Default is 0.02.
            
        Returns:
        - image_rgb : numpy.ndarray
            The output image after applying anisotropic diffusion, converted back to RGB format by stacking the 
            grayscale image across three channels.
        """
        # Convert RGB to grayscale if necessary
        if image.ndim == 3 and image.shape[2] == 3:
            image = np.dot(image[..., :3], [0.2989, 0.5870, 0.1140])  # RGB to grayscale conversion

        # Normalize image to 0-1 range
        image = image.astype(np.float32) / 255.0

        # Apply the diffusion process for a specified number of iterations
        for _ in range(ITER_PARAM):
            # Calculate gradients in all directions
            grad_north = np.roll(image, -1, axis=0) - image
            grad_south = np.roll(image, 1, axis=0) - image
            grad_east = np.roll(image, -1, axis=1) - image
            grad_west = np.roll(image, 1, axis=1) - image

            # Compute diffusion coefficients for each direction based on gradients
            c_north = np.exp(-(grad_north/K_PARAM)**2)
            c_south = np.exp(-(grad_south/K_PARAM)**2)
            c_east = np.exp(-(grad_east/K_PARAM)**2)
            c_west = np.exp(-(grad_west/K_PARAM)**2)

            # Update the image based on the diffusion coefficients and gradients
            image += ALPHA_PARAM * (c_north * grad_north + c_south * grad_south + c_east * grad_east + c_west * grad_west)

        # Scale back to 0-255 and convert to unsigned byte
        image = np.clip(image * 255, 0, 255).astype('uint8')
        # Convert grayscale back to RGB by stacking
        image_rgb = np.stack([image]*3, axis=-1)

        return image_rgb
        
    def load_dataset(self, path, split=0.2):
        """
        Loads the dataset from the specified path and splits it into
        training, validation, and test sets.
        
        Parameters:
        - path: The path to the dataset directory.
        - split: The fraction of the dataset to reserve for validation and test splits.
        
        Returns:
        - A tuple of tuples containing the training, validation, and test sets.
        """
        images = sorted(glob(os.path.join(path, "images", "*.png")))
        masks = sorted(glob(os.path.join(path, "masks", "*.png")))
        
        split_size = int(len(images) * split)
        
        train_x, valid_x = train_test_split(images, test_size=split_size, random_state=42)
        train_y, valid_y = train_test_split(masks, test_size=split_size, random_state=42)
        
        train_x, test_x = train_test_split(train_x, test_size=split_size, random_state=42)
        train_y, test_y = train_test_split(train_y, test_size=split_size, random_state=42)
        
        # Visualize and save images after dataset is loaded and split
        self.display_dataset(train_x, train_y, num_images=min(10, len(train_x)))
        
        return (train_x, train_y), (valid_x, valid_y), (test_x, test_y)
    
    def read_image(self, path, decode=False):
        """
        Reads an image from the specified path, applies preprocessing, and normalizes the pixel values.
        
        Parameters:
        - path: The path to the image file.
        - decode: Boolean flag to determine if the path needs decoding from bytes to string.
        - apply_diffusion: Whether to apply anisotropic diffusion to the image.
        - filter_type: The type of spatial filter to apply. If None, no filtering is applied.
        - morph_operation: The type of morphological operation to apply. If None, no operation is applied.
        
        Returns:
        - The processed image as a numpy array.
        """
        if decode:
            path = path.decode('utf-8')
        image = cv2.imread(path, cv2.IMREAD_COLOR)
        image = cv2.resize(image, (self.image_width, self.image_height))
        
        if ENABLED_DIFFUSION:
            image = self.apply_anisotropic_diffusion(image)
        
        if FILTER:
            image = self.apply_spatial_filtering(image, FILTER)
        
        if MORPH_OPERATION:
            image = self.apply_morphological_operations(image, MORPH_OPERATION)
        
        image = image / 255.0
        image = image.astype(np.float32)
        return image
    
    def read_mask(self, path, decode = False):
        """
        Reads a mask from the specified path, resizes it, and normalizes the pixel values.
        
        Parameters:
        - path: The path to the mask file.
        - decode: Boolean flag to determine if the path needs decoding from bytes to string.
        
        Returns:
        - The processed mask as a numpy array with an added dimension for compatibility.
        """
        if decode:
            path = path.decode('utf-8')
        x = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        x = cv2.resize(x, (self.image_width, self.image_height))
        x = x / 255.0
        x = x.astype(np.float32)
        x = np.expand_dims(x, axis=-1)
        return x
    
    def tf_parse(self, x, y):
        """
        Parses the image and mask file paths and returns the processed image and mask.
        
        Parameters:
        - x: The file path to the image.
        - y: The file path to the mask.
        
        Returns:
        - The processed image and mask tensors.
        """
        def _parse(x, y):
            x = self.read_image(x, True)
            y = self.read_mask(y, True)
            return x, y
        
        x, y = tf.numpy_function(_parse, [x, y], [tf.float32, tf.float32])
        x.set_shape([self.image_height, self.image_width, 3])
        y.set_shape([self.image_height, self.image_width, 1])
        return x, y
    
    def tf_dataset(self, X, Y, batch=2):
        """
        Creates a TensorFlow dataset from the given image and mask file paths.
        
        Parameters:
        - X: A list of image file paths.
        - Y: A list of mask file paths.
        - batch: The batch size for the dataset.
        
        Returns:
        - A TensorFlow dataset ready for model training.
        """
        dataset = tf.data.Dataset.from_tensor_slices((X, Y))
        dataset = dataset.map(self.tf_parse)
        dataset = dataset.batch(batch)
        dataset = dataset.prefetch(10)
        return dataset
    
    def display_dataset(self, X, Y, num_images=10):
        """
        Displays images and masks in a grid format for dataset validation
        and saves the grid as an image file.
        
        Parameters:
        - X: A list of image file paths.
        - Y: A list of mask file paths.
        - num_images: Number of images to display.
        """
        plt.figure(figsize=(20, 10))
        for i in range(num_images):
            img_path = X[i]
            mask_path = Y[i]
            img = self.read_image(img_path)
            mask = self.read_mask(mask_path)

            plt.subplot(2, num_images, i+1)
            plt.imshow(img)
            plt.axis('off')

            plt.subplot(2, num_images, num_images+i+1)
            plt.imshow(np.squeeze(mask), cmap='gray')
            plt.axis('off')

        plt.tight_layout()
        # Save the figure instead of showing it
        plt.savefig("dataset_preview.png")
        plt.close()